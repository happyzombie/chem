﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChemCalc
{
    public partial class Form1 : Form
    {

        ElementInfo EI;
        EquationUnpacker EU = new EquationUnpacker();
        Molecule M;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EI = new ElementInfo();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string symbol;
            double weight;
            if (EI.ESymbol.TryGetValue(textBox1.Text.ToLower(), out symbol) && EI.EWeight.TryGetValue(textBox1.Text.ToLower(), out weight))
            {
                lbl_sym.Text = symbol;
                lbl_weight.Text = ""+weight;
                textBox1.SelectAll();
            }
            else
            {
                lbl_sym.Text = "?";
                lbl_weight.Text = "0.0";
            }
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            string test = "";
            M = EU.UnpackEquation(textBox2.Text);
            
            if (M.quantity > 0)
            {
                String.Concat(test,"{0} lots of :\n\b",M.quantity);
            }
            foreach (KeyValuePair<string, int> entry in M.molecule)
            {
                Console.WriteLine("{0} : {1}" , entry.Key, entry.Value);
            }
        }
    }
}
