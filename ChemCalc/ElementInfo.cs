﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChemCalc
{
    public class ElementInfo
    {
        public string[] elements = {"hydrogen","helium","lithium","beryllium","boron","carbon","nitrogen","oxygen","fluorine","neon","sodium","magnesium","aluminium","silicon",
                                     "phosphorus","sulphur","chlorine","argon","potassium","calcium","scandium","titanium","vanadium","chromium","manganese","iron","cobalt","nickel",
                                     "copper","zinc","gallium","germanium","arsenic","selenium","bromine","krypton","rubidium","strontium","yttrium","zirconium","niobium","molybdenum",
                                     "technetium","ruthenium","rhodium","palladium","silver,","cadmium","indium","tin","antimony","tellurium","iodine","xenon","caesium","barium",
                                     "lanthanum","cerium","praseodymium","neodymium","promethium","samarium","europium","gadolinium","terbium","dysprosium","holmium","erbium","thulium",
                                     "ytterbium","lutetium","hafnium","tantalum","tungsten","rhenium","osmium","iridium","platinum","gold","mercury","thallium","lead","bismuth",
                                     "polonium","astatine","radon","francium","radium","actinium","thorium","protactinium","uranium","neptunium","plutonium","americium","curium",
                                     "berkelium","californium","einsteinium","fermium","mendelevium","nobelium","lawrencium","rutherfordium","dubnium","seaborgium","bohrium","hassium",
                                     "meitnerium","darmstadtium","roentgenium","copernicium","ununtrium","flerovium","ununpentium","livermorium","ununseptium","ununoctium"};
        public string[] symbols = {"H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si",
                                    "P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni",
                                    "Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr","Nb","Mo",
                                    "Tc","Ru","Rh","Pd","Ag,","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba",
                                    "La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm",
                                    "Yb","Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi",
                                    "Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm",
                                    "Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs",
                                    "Mt","Ds","Rg","Cn","Uut","Fl","Uup","Lv","Uus","Uuo" };
        public double[] weights = { 1.008,4.002602,6.94,9.0121831,10.81,12.011,14.007,15.999,18.998403163,20.1797,22.98976928,24.305,26.9815385,
                                    28.085,30.973761998,32.06,35.45,39.948,39.0983,40.078,44.955908,47.867,50.9415,51.9961,54.938044,55.845,
                                    58.933194,58.6934,63.546,65.38,69.723,72.630,74.921595,78.971,79.904,83.798,85.4678,87.62,88.90584,91.224,
                                    92.90637,95.95,97,101.07,102.90550,106.42,107.8682,112.414,114.818,118.710,121.760,127.60,126.90447,131.293,
                                    132.90545196,137.327,138.90547,140.116,140.90766,144.242,145,150.36,151.964,157.25,158.92535,162.500,164.93033,
                                    167.259,168.93422,173.045,174.9668,178.49,180.94788,183.84,186.207,190.23,192.217,195.084,196.966569,200.592,
                                    204.38,207.2,208.98040,209,210,222,223,226,227,232.0377,231.03588,238.02891,237,244,243,247,247,251,252,
                                    257,258,259,262,267,270,271,270,277,276,281,282,285,285,289,289,293,294,294 };
        public Dictionary<string, double> EWeight = new Dictionary<string, double>();
        public Dictionary<string, string> ESymbol = new Dictionary<string, string>();

        public ElementInfo()
        {
            for (int i = 0; i < elements.Length; i++)
            {
                //Console.WriteLine("{0} {1} {2}", elements[i], symbols[i], weights[i]);
                EWeight.Add(elements[i], weights[i]);
                ESymbol.Add(elements[i], symbols[i]);
            }
        }
    }

    public class Molecule
    {
        public Dictionary<string, int> molecule { get; }
        public int quantity { get; }

        public Molecule(int quantity, Dictionary<string, int> molecule){
            this.quantity = quantity;
            this.molecule = molecule;
        }
    }

    public class EquationUnpacker
    {
        private Dictionary<string, int> atoms = new Dictionary<string, int>();


        private Tuple<int, int> ParseNumbers(string toParse, int index)
        {
            string NumString = "";
            for (int i = index; i < toParse.Length; i++)
            {
                char c = toParse[i];
                if (c.IsNum())
                {
                    NumString += c;
                }
                else
                {
                    index = i - 1;
                    break;
                }
            }
            return new Tuple<int, int>(int.Parse(NumString), index);
        }
        //END ParseNumbers


        //---------------
        //Inserts element info into the atoms dictionary
        private void InsertToDict(string currentAtom, int value)
        {
            if (atoms.ContainsKey(currentAtom))
            {
                atoms[currentAtom] += value;
            }
            else
            {
                atoms.Add(currentAtom, value);
            }
        }
        //END InsertToDict


        //Main
        //Main function 
        public Molecule UnpackEquation(string chem)
        {
            Tuple<int, Dictionary<string, int>> lel;
            string currentAtom = "";
            int multiplier = -1;

            Tuple<int, int> temp;
            for (int i = 0; i < chem.Length; ++i)
            {
                char c = chem[i];
                if (c.IsNum())
                {
                    temp = ParseNumbers(chem, i);
                    if (currentAtom != "")
                    {
                        InsertToDict(currentAtom, temp.Item1);
                    }
                    else
                    {
                        multiplier = temp.Item1;
                    }
                    i = temp.Item2;
                    currentAtom = "";
                }
                else
                {
                    if (!c.IsLower())
                    {
                        if (currentAtom != "")
                        {
                            InsertToDict(currentAtom, 1);
                            currentAtom = "" + c;
                        }
                        else
                        {
                            currentAtom += c;
                        }

                    }
                    else
                    {
                        currentAtom += c;
                    }
                }
            }
            if (currentAtom != "")
            {
                InsertToDict(currentAtom, 1);
            }

            return new Molecule(multiplier, atoms);
        }
    }
}


