﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChemCalc
{
    public static class Extension
    {
        public static bool IsNum(this char c)
        {
            return ((int)c >= 48 && (int)c <= 57) ? true : false;
        }
        public static bool IsLower(this char c)
        {
            return ((int)c >= 97 && (int)c <= 122) ? true : false;
        }
        public static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }
    }
}


